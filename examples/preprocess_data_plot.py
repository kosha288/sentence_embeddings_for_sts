"""
This basic example loads a pre-trained model from the web and uses it to
generate sentence embeddings for a given list of sentences.
"""

from sentence_transformers import SentenceTransformer, LoggingHandler
import numpy as np
import logging
from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from scipy.spatial import distance
import json
from sklearn.cluster import AgglomerativeClustering
#### Just some code to print debug information to stdout
np.set_printoptions(threshold=100)

logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
#### /print debug information to stdout


stop_words = set(stopwords.words('english'))
def preprocess_sentence(text):
    text = text.replace('/', ' / ')
    text = text.replace('.-', ' .- ')
    text = text.replace('.', ' . ')
    text = text.replace('\'', ' \' ')
    text = text.lower()

    tokens = [token for token in word_tokenize(text) if token not in punctuation ]

    return ' '.join(tokens)


# Load Sentence model (based on BERT) from URL
# model = SentenceTransformer('bert-large-nli-stsb-mean-tokens')

# model = SentenceTransformer('bert-base-wikipedia-sections-mean-tokens')


model = SentenceTransformer('output/training_stsbenchmark_bert-2020-01-29_10-23-28')

# model = SentenceTransformer('output/training_stsbenchmark_xlnet-2020-02-13_09-15-27')
# model = SentenceTransformer('output/xlnet_cased_L-24_H-1024_A-16')

# model = SentenceTransformer('output/training_clf_bert-2020-02-27_00-04-08')
# model = SentenceTransformer('bert-large-nli-stsb-mean-tokens')

# Embed a list of sentences

sentence = preprocess_sentence('Breast cancers with HER2 amplification have a higher risk of CNS metastasis and poorer prognosis.')
# print(sentence)
# sentences=[]
# with open('data_sample', encoding="utf8") as f:
#     for s in f.readlines():
#         # print(s)
#         data = json.loads(s)
#         # if (data['title'] is not '') and (data['fieldsOfStudy'] is not []):
#         #     sentences.append(tuple((data['title'],data['fieldsOfStudy'])))
#         if (data['paperAbstract'] is not '') and (data['fieldsOfStudy'] != []):
#             sentences.append(tuple((preprocess_sentence(data['paperAbstract']),data['fieldsOfStudy'][0])))
#
# total=len(sentences)
# print('total no of samples ',total)
# temp='{"entities":[],"journalVolume":"","journalPages":"","pmid":"","year":2007,"outCitations":[],"s2Url":"https://semanticscholar.org/paper/5a34004d30396ba61bdc9be2afdafd2dbcc291a9","s2PdfUrl":"","id":"5a34004d30396ba61bdc9be2afdafd2dbcc291a9","authors":[{"name":"Benoît Raymond","ids":["35085909"]},{"name":"Lhousseine Touqui","ids":["3752118"]}],"journalName":"","paperAbstract":"Among the enzymes involved in the hydrolysis of membrane phospholipids, type-IIA secreted phospholipase A2 (sPLA2-IIA) plays a major role in the development of a number of inflammatory diseases. Indeed, this enzyme has been shown to release arachidonic acid, the precursor of pro-inflammatory eicosanoids, and to hydrolyze phospholipids of pulmonary surfactant. This hydrolysis, which alters the tensioactive properties of surfactant phospholipids, leading to alveolar collapse, plays a critical role in the pathogenesis of acute respiratory distress syndrome. sPLA2-IIA has also been shown to bind to specific receptors located on cell surface membranes, although the biological significance of this binding remains unclear. However, the most well-established biological role of sPLA2-IIA is related to its potent bactericidal properties compared to other sPLA2s. This bactericidal effect is due to the ability of sPLA2-IIA to bind to and penetrate the cell wall of bacteria, especially those of Gram-positive bacteria....","inCitations":[],"pdfUrls":[],"title":"Host defense against Bacillus anthracis and its subversion by anthrax toxins: a role for type-IIA secreted phospholipase A2","doi":"10.1080/17471060601152224","sources":[],"doiUrl":"https://doi.org/10.1080/17471060601152224","venue":""}'

# temp_dict=json.loads(data)
# pa=(temp_dict['paperAbstract'])
# pa2='We have determined the effects of coexpression of Kv2.1 with electrically silent Kv5.1 or Kv6.1 α-subunits in Xenopus oocytes on channel gating. Kv2.1/5.1 selectively accelerated the rate of inactivation at intermediate potentials (-30 to 0 mV), without affecting the rate at strong depolarization (0 to +40 mV), and markedly accelerated the rate of cumulative inactivation evoked by high-frequency trains of short pulses. Kv5.1 coexpression also slowed deactivation of Kv2.1. In contrast, Kv6.1 was much less effective in speeding inactivation at intermediate potentials, had a slowing effect on inactivation at strong depolarizations, and had no effect on cumulative inactivation. Kv6.1, however, had profound effects on activation, including a negative shift of the steady-state activation curve and marked slowing of deactivation tail currents. Support for the notion that the Kv5.1 s effects stem from coassembly of α-subunits into heteromeric channels was obtained from biochemical evidence of protein-protein interaction and single-channel measurements that showed heterogeneity in unitary conductance. Our results show that Kv5.1 and Kv6.1 function as regulatory α-subunits that when coassembled with Kv2.1 can modulate gating in a physiologically relevant manner.'
# pa=preprocess_sentence(pa)
# pa2=preprocess_sentence(pa2)
# sentences = ['This framework generates embeddings for each input sentence',
#              'Sentences are passed as a list of string.',
#              'A man is eating a food.',
#              'A man is ridding bicycle.',
#              'i love to read books.',
#              'I enjoyed the documentery.',
#              'You come here to study not to play',
#              'You come here to play not to study']

sentences = [
             'the cosmetics have to be exposed to air or contacted with fingers during they have been used three kinds of general cosmetics and nanometal-containing cosmetics were poured into petri dish and exposed to air for 24 hr and then mixed with a culture medium in which bacteria were not grown during cultivating for 48 hr three kinds of general cosmetics and nanometal-containing cosmetics were contaminated artificially by stirring with fingers and incubated at 30oc for 10 days when the contaminated cosmetics were mixed with the culture medium various bacteria grew the bacteria grown in the mixture of cosmetics and culture medium were identified with escherichia coli micrococcus luteus kocuria rosea dermacoccus sp bacillus thringiensis staphyllococcus sp and paenibacillus campinasensis streptomyces sp bacillus amyloliquefaciens based on the genbank database and 16s-rdna sequence homology the bacteria contaminated the cosmetics are not pathogenic but found generally in soils waters and around human life bacterial cell number grown in the general cosmetics and nanometal-containing cosmetics by the artificial contamination was 50~90 per g of cosmetic and 4~26 per g of cosmetic respectively these results present a clue that nano-sized platinum or silver particle contained in the nanometal-containing cosmetics may influence negatively on bacterial growth conclusively the cosmetics are difficult to be contaminated by air-contacting however actively contaminated by the finger-contacting during using Biology',
             'thank you very much for downloading residential energy cost savings and comfort for existing buildings maybe you have knowledge that people have search hundreds times for their favorite books like this residential energy cost savings and comfort for existing buildings but end up in malicious downloads rather than reading a good book with a cup of coffee in the afternoon instead they cope with some infectious virus inside their laptop Computer Science',
             'advice and support for consumers in the royal borough is provided by the citizens advice consumer service Business',
             'man findet bei frauen mit auserordentlich schlaffen bauchdecken oder bei individuen mit grosen bauchwandhernien im rechten bauchraum zuweilen einen glatten tumor der sich bei bimanueller untersuchung als der leber zugehorig erweist und eine gute verschieblichkeit zeigt untersucht man im stehen so ruckt der tumor tief nach abwarts so das er fast der darmbeinschaufel aufzuliegen scheint drangt man den tumor nach oben was unschwer gelingt so fallt er beim nachsten atemzug wieder nach abwarts in liegender stellung kann man den tumor mit beiden handen fassen und uber das niveau der vorderen bauchwand vorwalzen abb 35 eine genaue topographische orientierung fallt nicht leicht denn es ist oft schwierig an diesem tumor den rand oder gar die einzelnen inzisuren zu unterscheiden im stehenden zustande kann perkutorisch die leberdampfung verschwinden besonders im epigastrium und in der rechten parasternallinie ubt man jetzt von unten her auf den herabgesunkenen tumor einen druck so last er sich meist hinter den rippenbogen hinaufdrangen und entzieht sich so der palpation gleichzeitig damit tritt die normale leberdampfung wieder auf History	',
             'in order to determine the effect of various hosts on the feeding performance of the tick hyalomma truncatum we used three mammalian species as hosts larvae and nymphs of h truncatum were fed under controlled laboratory conditions on gerbils guinea-pigs and rabbits the larvae fed for 4 3Â±1 4 days on gerbils 5 6Â±1 3 days on guinea-pigs and 4 7Â±1 2 days on rabbits the mean weights of the larvae which fed on the rabbits guinea-pigs and gerbils were 0 58Â± 0 09 0 46Â±0 04 and 0 45Â±0 04 mg respectively the feeding periods of the nymphs on gerbils guinea-pigs and rabbits were 7 9Â±1 3 8 6Â±1 3 and 9 6Â±2 2 days respectively the mean weights of the nymphs which fed on the gerbils guinea-pigs and rabbits were 22 5Â±2 8 19 7Â±1 3 and 15 8Â±1 4 mg respectively hyalomma truncatum demonstrated a life cycle of a three-host tick on gerbils and guinea-pigs and of a two-host tick on rabbits the evolutionary advantage of a two-host cycle over a three-host cycle in metastriate ticks is discussed Biology',
             'aza-claisen rearrangements 3-aza-cope rearrangements have gained an in- creasing interest in synthetic organic chemistry originally the exceptionally high reaction temperatures of this hetero variant of the well-known 3,3-sigmatropic reaction limited their applicability to selected molecules since about 1970 charge acceleration enabled a signifi- cant reduction of the reaction temperature to be achieved and cation- and anion-promoted rearrangements found their way into the syntheses of more complex molecules the first to- tal syntheses of natural products were reported the development of zwitterionic aza-claisen rearrangements allowed the reactions to be run at room temperature or below and the charge neutralization served as the highly efficient driving force after overcoming several teething troubles the method was established as a reliable conversion displaying various stereochemical advantages the first successful total syntheses of natural products incorpo- rating the aza-claisen rearrangement as a key step emphasized the synthetic potential to date the aza-claisen rearrangements are far from being exhausted still an enantioselectively catalyzed variant has to be developed this review summarizes one decade of investigation efforts in this area Chemistry',
             'from 2009 the market of smart devices has been rapidly increasing these devices provide various services to users the cloud messaging service especially is applied to many various services and sends messages asynchronously in the cloud messaging service there are two methods for message transmission message transmission based on an ip address and a publish subscribe technique each technique uses basic messages in order to send messages to mobile devices in this paper the hybrid message transmission based on user-customized analysis to reduce basic messages is proposed the hybrid message transmission uses exponential moving average ema and k-means algorithms for user-customized analysis and determines the message transmission techniques in each timeslot Computer Science ',
             'using a nationwide sample of large cities this article analyzes changes in the use of enterprise funds during the past decade the major findings are that 1 the aggregate number of enterprise funds increased with the largest increases occurring in solid waste and drainage 2 part of the increase was offset by the elimination of some enterprise funds particularly in the area of recreational services 3 60 percent of the cities experienced one or more changes in the types of enterprise funds they used 4 the revenues associated with most types of enterprise funds have increased at a faster rate than general fund revenues and 5 some cities are using alternative fiscal structures e g special revenue funds and discrete component units to account for services that are reported as enterprise funds in other cities a continuum of fiscal structures is presented as a framework for addressing why cities might choose one structure over another and what the possible implications of a particular fiscal structure might be Economics',
             'there is provided a device for wireless audio-video transmission this device can implement the requested audio-video signal from the cable line into a wireless signal to send the receivers that are located within the transmission range can receive the requested wireless signal Computer Science',
             'since the fall in eden the race has been degenerating deformity imbecility disease and human suffering have been pressing heavier and heavier upon each successive generation since the fall and yet the masses are asleep as to the real causes they do not consider that they themselves are guilty in a great measure for this deplorable state of things they generally charge their sufferings upon providence and regard god as the author of their woes but it is intemperance to a greater or less degree that lies at the foundation of all this suffering p 1 para History',
             ]

import tensorflow as tf
# import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf
import pandas as pd
import re
import seaborn as sns
from sklearn.metrics.pairwise import cosine_similarity

def plot_similarity(labels, features, rotation):

  corr = np.inner(cosine_similarity(features, features),cosine_similarity(features, features))
  # corr = np.inner(features, features)
  sns.set(font_scale=1.2)
  g = sns.heatmap(
      corr,
      xticklabels=labels,
      yticklabels=labels,
      vmin=0,
      vmax=1,
      cmap="YlGnBu")
  g.set_xticklabels(labels, rotation=rotation)
  g.set_title("Semantic Textual Similarity")
  print('heatmap displayed...')



# processed_sentence=[]
# for s in sentences:
#     processed_sentence.append(preprocess_sentence(s))
# sentence_text = [row[0] for row in sentences]
# sentence_embeddings = model.encode(sentence_text)


sentence_embeddings = model.encode(sentences)
print('the shape of embedding is ')
print(sentence_embeddings[0].shape)
plot_similarity(sentences, sentence_embeddings,90)


# num_clusters = len(set([tuple(row[1]) for row in sentences]))
# # km = AgglomerativeClustering(distance_threshold=0.5,n_clusters=None)
# km = AgglomerativeClustering(n_clusters=num_clusters)
# km.fit(sentence_embeddings)
#
# cluster_assignment = km.labels_
# # num_clusters= len(cluster_assignment)
#
# import matplotlib.pyplot as plt
# plt.plot(sentence_embeddings,cluster_assignment)
# plt.show()
#
# clustered_sentences = [[] for i in range(num_clusters)]
# for sentence_id, cluster_id in enumerate(cluster_assignment):
#     clustered_sentences[cluster_id].append(sentences[sentence_id])
#
# for i, cluster in enumerate(clustered_sentences):
#     print("Cluster ", i+1)
#     for row in cluster:
#         print("(Gold label: {}) - {}".format(row[1], row[0]))
#     print("")



# from sklearn.cluster import KMeans
# import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()  # for plot styling
# import numpy as np
#
# kmeans = KMeans(n_clusters=10)
# kmeans.fit(sentence_embeddings)
# y_kmeans = kmeans.predict(sentence_embeddings)
# plt.scatter(sentence_embeddings, c=y_kmeans, s=50, cmap='viridis')

# centers = kmeans.cluster_centers_
# plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5);


# results=km.labels_
# print('\n no of clusters ',len(results))
# for i in range(total):
#     print(results[i],' for text',sentences[i],'\n')
# print(model.encode(pa))
# The result is a list of sentence embeddings as numpy arrays
# for sentence, embedding in zip(sentences, sentence_embeddings):
#     print("Sentence:", sentence)
#     print("Embedding:", embedding)
#     print("")
#
# print('the shape of embedding is ')
# print(sentence_embeddings[0].shape)

# cosine_sim = 1 - distance.cosine(model.encode(pa2)[0], model.encode(pa)[0])
# # cosine_sim = 1 - distance.cosine(sentence_embeddings[6], sentence_embeddings[7])
# print('cosine similarity:', cosine_sim)
