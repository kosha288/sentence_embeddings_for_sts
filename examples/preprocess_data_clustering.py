"""
This basic example loads a pre-trained model from the web and uses it to
generate sentence embeddings for a given list of sentences.
"""

from sentence_transformers import SentenceTransformer, LoggingHandler
import numpy as np
import logging
from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from scipy.spatial import distance
import json
from sklearn.cluster import AgglomerativeClustering
#### Just some code to print debug information to stdout
np.set_printoptions(threshold=100)

logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
#### /print debug information to stdout


stop_words = set(stopwords.words('english'))
def preprocess_sentence(text):
    text = text.replace('/', ' / ')
    text = text.replace('.-', ' .- ')
    text = text.replace('.', ' . ')
    text = text.replace('\'', ' \' ')
    text = text.lower()

    tokens = [token for token in word_tokenize(text) if token not in punctuation ]

    return ' '.join(tokens)


# Load Sentence model (based on BERT) from URL
# model = SentenceTransformer('bert-large-nli-stsb-mean-tokens')

# model = SentenceTransformer('bert-base-wikipedia-sections-mean-tokens')


model = SentenceTransformer('output/training_stsbenchmark_bert-2020-01-29_10-23-28')

# model = SentenceTransformer('output/training_stsbenchmark_xlnet-2020-02-13_09-15-27')
# model = SentenceTransformer('output/xlnet_cased_L-24_H-1024_A-16')

# model = SentenceTransformer('output/training_clf_bert-2020-02-27_00-04-08')
# model = SentenceTransformer('bert-large-nli-stsb-mean-tokens')

# Embed a list of sentences

sentence = preprocess_sentence('Breast cancers with HER2 amplification have a higher risk of CNS metastasis and poorer prognosis.')
# print(sentence)
sentences=[]
with open('data_sample', encoding="utf8") as f:
    for s in f.readlines():
        # print(s)
        data = json.loads(s)
        # if (data['title'] is not '') and (data['fieldsOfStudy'] is not []):
        #     sentences.append(tuple((data['title'],data['fieldsOfStudy'])))
        if (data['paperAbstract'] is not '') and (data['fieldsOfStudy'] != []):
            sentences.append(tuple((preprocess_sentence(data['paperAbstract']),data['fieldsOfStudy'][0])))

total=len(sentences)
print('total no of samples ',total)

# import seaborn as sns
# from sklearn.metrics.pairwise import cosine_similarity
#
# def plot_similarity(labels, features, rotation):
#
#   # corr = np.inner(features, features)
#
#   corr = np.inner(cosine_similarity(features, features),cosine_similarity(features, features))
#   sns.set(font_scale=0.8)
#   g = sns.heatmap(
#       corr,
#       xticklabels=labels,
#       yticklabels=labels,
#       vmin=0,
#       vmax=1,
#       cmap="YlOrRd",
#       annot=True)
#   g.set_xticklabels(labels, rotation=rotation)
#   g.set_title("Semantic Textual Similarity")

# temp='{"entities":[],"journalVolume":"","journalPages":"","pmid":"","year":2007,"outCitations":[],"s2Url":"https://semanticscholar.org/paper/5a34004d30396ba61bdc9be2afdafd2dbcc291a9","s2PdfUrl":"","id":"5a34004d30396ba61bdc9be2afdafd2dbcc291a9","authors":[{"name":"Benoît Raymond","ids":["35085909"]},{"name":"Lhousseine Touqui","ids":["3752118"]}],"journalName":"","paperAbstract":"Among the enzymes involved in the hydrolysis of membrane phospholipids, type-IIA secreted phospholipase A2 (sPLA2-IIA) plays a major role in the development of a number of inflammatory diseases. Indeed, this enzyme has been shown to release arachidonic acid, the precursor of pro-inflammatory eicosanoids, and to hydrolyze phospholipids of pulmonary surfactant. This hydrolysis, which alters the tensioactive properties of surfactant phospholipids, leading to alveolar collapse, plays a critical role in the pathogenesis of acute respiratory distress syndrome. sPLA2-IIA has also been shown to bind to specific receptors located on cell surface membranes, although the biological significance of this binding remains unclear. However, the most well-established biological role of sPLA2-IIA is related to its potent bactericidal properties compared to other sPLA2s. This bactericidal effect is due to the ability of sPLA2-IIA to bind to and penetrate the cell wall of bacteria, especially those of Gram-positive bacteria....","inCitations":[],"pdfUrls":[],"title":"Host defense against Bacillus anthracis and its subversion by anthrax toxins: a role for type-IIA secreted phospholipase A2","doi":"10.1080/17471060601152224","sources":[],"doiUrl":"https://doi.org/10.1080/17471060601152224","venue":""}'

# temp_dict=json.loads(data)
# pa=(temp_dict['paperAbstract'])
# pa2='We have determined the effects of coexpression of Kv2.1 with electrically silent Kv5.1 or Kv6.1 α-subunits in Xenopus oocytes on channel gating. Kv2.1/5.1 selectively accelerated the rate of inactivation at intermediate potentials (-30 to 0 mV), without affecting the rate at strong depolarization (0 to +40 mV), and markedly accelerated the rate of cumulative inactivation evoked by high-frequency trains of short pulses. Kv5.1 coexpression also slowed deactivation of Kv2.1. In contrast, Kv6.1 was much less effective in speeding inactivation at intermediate potentials, had a slowing effect on inactivation at strong depolarizations, and had no effect on cumulative inactivation. Kv6.1, however, had profound effects on activation, including a negative shift of the steady-state activation curve and marked slowing of deactivation tail currents. Support for the notion that the Kv5.1 s effects stem from coassembly of α-subunits into heteromeric channels was obtained from biochemical evidence of protein-protein interaction and single-channel measurements that showed heterogeneity in unitary conductance. Our results show that Kv5.1 and Kv6.1 function as regulatory α-subunits that when coassembled with Kv2.1 can modulate gating in a physiologically relevant manner.'
# pa=preprocess_sentence(pa)
# pa2=preprocess_sentence(pa2)
# sentences = ['This framework generates embeddings for each input sentence',
#              'Sentences are passed as a list of string.',
#              'A man is eating a food.',
#              'A man is ridding bicycle.',
#              'i love to read books.',
#              'I enjoyed the documentery.',
#              'You come here to study not to play',
#              'You come here to play not to study']
#
# processed_sentence=[]
# for s in sentences:
#     processed_sentence.append(preprocess_sentence(s))
sentence_text = [row[0] for row in sentences]
sentence_embeddings = model.encode(sentence_text)

# plot_similarity(sentence_text, sentence_embeddings,90)

num_clusters = len(set([tuple(row[1]) for row in sentences]))
# km = AgglomerativeClustering(distance_threshold=0.5,n_clusters=None)
km = AgglomerativeClustering(n_clusters=num_clusters)
km.fit(sentence_embeddings)

cluster_assignment = km.labels_
# num_clusters= len(cluster_assignment)

# import matplotlib.pyplot as plt
# plt.plot(sentence_embeddings,cluster_assignment)
# plt.show()

clustered_sentences = [[] for i in range(num_clusters)]
for sentence_id, cluster_id in enumerate(cluster_assignment):
    clustered_sentences[cluster_id].append(sentences[sentence_id])

for i, cluster in enumerate(clustered_sentences):
    print("Cluster ", i+1)
    for row in cluster:
        print("(Gold label: {}) - {}".format(row[1], row[0]))
    print("")



# from sklearn.cluster import KMeans
# import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()  # for plot styling
# import numpy as np
#
# kmeans = KMeans(n_clusters=10)
# kmeans.fit(sentence_embeddings)
# y_kmeans = kmeans.predict(sentence_embeddings)
# plt.scatter(sentence_embeddings, c=y_kmeans, s=50, cmap='viridis')

# centers = kmeans.cluster_centers_
# plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5);


# results=km.labels_
# print('\n no of clusters ',len(results))
# for i in range(total):
#     print(results[i],' for text',sentences[i],'\n')
# print(model.encode(pa))
# The result is a list of sentence embeddings as numpy arrays
# for sentence, embedding in zip(sentences, sentence_embeddings):
#     print("Sentence:", sentence)
#     print("Embedding:", embedding)
#     print("")
#
# print('the shape of embedding is ')
# print(sentence_embeddings[0].shape)

# cosine_sim = 1 - distance.cosine(model.encode(pa2)[0], model.encode(pa)[0])
# # cosine_sim = 1 - distance.cosine(sentence_embeddings[6], sentence_embeddings[7])
# print('cosine similarity:', cosine_sim)
