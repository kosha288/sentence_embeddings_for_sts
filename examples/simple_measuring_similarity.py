"""
This basic example loads a pre-trained model from the web and uses it to
generate sentence embeddings for a given list of sentences.
"""

from sentence_transformers import SentenceTransformer, LoggingHandler
import numpy as np
import logging
from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from scipy.spatial import distance

#### Just some code to print debug information to stdout
np.set_printoptions(threshold=100)

logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
#### /print debug information to stdout


stop_words = set(stopwords.words('english'))
def preprocess_sentence(text):
    text = text.replace('/', ' / ')
    text = text.replace('.-', ' .- ')
    text = text.replace('.', ' . ')
    text = text.replace('\'', ' \' ')
    text = text.lower()

    #tokens = [token for token in word_tokenize(text) if token not in punctuation and token not in stop_words]
    tokens = [token for token in word_tokenize(text) if token not in punctuation ]

    return ' '.join(tokens)


# Load Sentence model (based on BERT) from URL
# model = SentenceTransformer('bert-base-nli-mean-tokens')
# model = SentenceTransformer('output/training_stsbenchmark_bert-2020-01-29_10-23-28')
# model = SentenceTransformer('output/training_stsbenchmark_xlnet-2020-02-13_09-15-27')
# model = SentenceTransformer('output/xlnet_cased_L-24_H-1024_A-16')
# model = SentenceTransformer('')
model = SentenceTransformer('bert-large-nli-stsb-mean-tokens')

# Embed a list of sentences

sentence = preprocess_sentence('Breast cancers with HER2 amplification have a higher risk of CNS metastasis and poorer prognosis.')
print(sentence)

sentences = ['This framework generates embeddings for each input sentence',
             'Sentences are passed as a list of string.',
             'A man is eating a food.',
             'A man is ridding bicycle.',
             'i love to read books.',
             'I enjoyed the documentery.',
             'You come here to study not to play',
             'You come here to play not to study']
processed_sentence=[]
for s in sentences:
    processed_sentence.append(preprocess_sentence(s))

sentence_embeddings = model.encode(processed_sentence)

# The result is a list of sentence embeddings as numpy arrays
# for sentence, embedding in zip(sentences, sentence_embeddings):
#     print("Sentence:", sentence)
#     print("Embedding:", embedding)
#     print("")

print('the shape of embedding is ')
print(sentence_embeddings[0].shape)

cosine_sim = 1 - distance.cosine(sentence_embeddings[6], sentence_embeddings[7])
print('cosine similarity:', cosine_sim)
