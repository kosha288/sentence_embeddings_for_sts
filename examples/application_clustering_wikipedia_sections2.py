"""
This examples clusters different sentences that come from the same wikipedia article.

It uses the 'wikipedia-sections' model, a model that was trained to differentiate if two sentences from the
same article come from the same section or from different sections in that article.
"""
from sentence_transformers import SentenceTransformer
from sklearn.cluster import AgglomerativeClustering



embedder = SentenceTransformer('bert-base-wikipedia-sections-mean-tokens')

#Sentences and sections are from Wikipeda.
#Source: https://en.wikipedia.org/wiki/Bushnell,_Illinois
corpus = [
("Low temperature, drought, and high salinity are common stress conditions that adversely affect plant growth and crop production. The cellular and molecular responses of plants to environmental stress have been studied intensively ", "biology"),
("The rice (Oryza sativa L.) metallothionein gene OsMT-I-4b has previously been identified as a type I MT gene. To elucidate the regulatory mechanism involved in its tissue specificity and abiotic induction, we isolated a 1 730 bp fragment of the OsMT-I-4b promoter region. Histochemical β-glucuronidase (GUS) staining indicated a precise spacial and temporal expression pattern in transgenic Arabidopsis.", "biology"),

("The scientific study of networks, including computer networks, social networks, and biological networks, has received an enormous amount of interest in the last few years. The rise of the Internet and the wide availability of inexpensive computers have made it possible to gather and analyze network data on a large scale", "Networking"),
("We introduce a new class of problems called network information flow which is inspired by computer network applications. Consider a point-to-point communication network on which a number of information sources are to be multicast to certain sets of destinations. We assume that the information sources are mutually independent. ", "Networking"),
("Network use has evolved to be dominated by content distribution and retrieval, while networking technology still speaks only of connections between hosts. Accessing content and services requires mapping from the what that users care about to the network's where.", "Networking"),

("Report summarizing climate change issues in 2013, with an emphasis on physical science. It includes a summary of issues to assist policymakers, a technical summary, and a list of frequently-asked questions. ", "environment"),
("Global climate change is having long-term impacts on the geographic distribution of forest species. However, the response of vertical belts of mountain forests to climate change is still little known.", "environment"),
("Many studies suggest that global warming is driving species ranges poleward and toward higher elevations at temperate latitudes, but evidence for range shifts is scarce for the tropics, where the shallow latitudinal temperature gradient makes upslope shifts more likely than poleward shifts.", "environment"),

("From 1991 to 2012, Bushnell was home to one of the largest Christian Music and Arts festivals in the world, known as the Cornerstone Festival.", "Music"),
("Each year around the 4th of July, 25,000 people from all over the world would descend on the small farm town to watch over 300 bands, authors and artists perform at the Cornerstone Farm Campgrounds.", "Music"),
("The festival was generally well received by locals, and businesses in the area would typically put up signs welcoming festival-goers to their town.", "Music"),
("As a result of the location of the music festival, numerous live albums and videos have been recorded or filmed in Bushnell, including the annual Cornerstone Festival DVD. ", "Music"),
("Cornerstone held its final festival in 2012 and no longer operates.", "Music"),

("Beginning in 1908, the Truman Pioneer Stud Farm in Bushnell was home to one of the largest horse shows in the Midwest.", "Horse show"),
("The show was well known for imported European horses.", "Horse show"),
("The Bushnell Horse Show features some of the best Belgian and Percheron hitches in the country. Teams have come from many different states and Canada to compete.", "Horse show"),
]

sentences = [row[0] for row in corpus]

corpus_embeddings = embedder.encode(sentences)
num_clusters = len(set([row[1] for row in corpus]))

#Sklearn clustering
km = AgglomerativeClustering(n_clusters=num_clusters)
km.fit(corpus_embeddings)

cluster_assignment = km.labels_


clustered_sentences = [[] for i in range(num_clusters)]
for sentence_id, cluster_id in enumerate(cluster_assignment):
    clustered_sentences[cluster_id].append(corpus[sentence_id])

for i, cluster in enumerate(clustered_sentences):
    print("Cluster ", i+1)
    for row in cluster:
        print("(Gold label: {}) - {}".format(row[1], row[0]))
    print("")

